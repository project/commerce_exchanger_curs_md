<?php

namespace Drupal\commerce_exchanger_curs_md\Plugin\Commerce\ExchangerProvider;

use Drupal\commerce_exchanger\Plugin\Commerce\ExchangerProvider\ExchangerProviderRemoteBase;
use Drupal\Component\Serialization\Json;

/**
 * Provides the Curs.md exchange rates.
 *
 * @CommerceExchangerProvider(
 *   id = "curs_md",
 *   label = "Curs.md",
 *   display_label = "Curs.md",
 *   historical_rates = TRUE,
 *   base_currency = "MDL",
 *   refresh_once = TRUE,
 *   transform_rates=TRUE,
 * )
 */
class CursMdExchanger extends ExchangerProviderRemoteBase{
  
  /**
   * {@inheritdoc}
   */
  public function apiUrl() {
    return "https://www.curs.md/ro/json_convertor_provider";
  }
  /**
   * {@inheritdoc}
   */
  public function getRemoteData($base_currency = NULL) {
    $request = $this->apiClient([]);
    $data = NULL;
    if($request){
      $rates = Json::decode($request);
      $data['base'] = "MDL";
      $rates = $rates['bnm']['rates'];
      foreach ($rates as $k => $rate){
        $rates[$k] = 1.0 / $rate['sell'];
      }
      $data['rates'] = $rates;
    }
    return $data;
  }

}
